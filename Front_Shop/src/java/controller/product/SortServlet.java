/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.product;

import dal.CategoriesDAO;
import dal.ProductDAO;
import dal.ProductImageDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import models.Categories;
import models.Product;
import models.ProductImage;

/**
 *
 * @author ACER
 */
public class SortServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SortServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SortServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        CategoriesDAO cdb = new CategoriesDAO();
        ProductDAO pdb = new ProductDAO();
        List<Categories> list = cdb.getAll();
        List<ProductImage> list_img = new ArrayList<>();
        List<Product> list1 = pdb.getAll();

        //lay tung anh
        ProductImageDAO pidb = new ProductImageDAO();
        for (int i = 1; i <= list1.size(); i++) {
            list_img.add(pidb.getImageById(i));
        }
        //phan trang
        String caId = request.getParameter("caID");
        request.setAttribute("caID", caId);
        //sort gia theo tung category
        int ca;
        String sortpro = request.getParameter("sort_product");
        request.setAttribute("sort", sortpro);
        switch (sortpro) {
            case "new":
                try {
                ca = (caId == null) ? 0 : Integer.parseInt(caId);

                List<Product> listall;
                listall = pdb.getProductByCategory(ca);

                int page, numberpage = 6;
                int size = listall.size();
                int num = (size % numberpage == 0 ? (size / numberpage) : ((size / numberpage) + 1));
                String xpage = request.getParameter("page");
                if (xpage == null) {
                    page = 1;
                } else {
                    page = Integer.parseInt(xpage);
                }
                int start, end;
                start = (page - 1) * numberpage;
                end = Math.min(page * numberpage, size);
                List<Product> listpage = pdb.getListByPage(listall, start, end);
                request.setAttribute("listpage", listpage);
                request.setAttribute("page", page);
                request.setAttribute("num", num);
                request.setAttribute("size", size);
            } catch (NumberFormatException e) {
                System.out.println(e);
            }
            break;
            case "low":
                try {
                ca = (caId == null) ? 0 : Integer.parseInt(caId);

                List<Product> listall;

                listall = pdb.sort(ca, sortpro);

                int page, numberpage = 6;
                int size = listall.size();
                int num = (size % numberpage == 0 ? (size / numberpage) : ((size / numberpage) + 1));
                String xpage = request.getParameter("page");
                if (xpage == null) {
                    page = 1;
                } else {
                    page = Integer.parseInt(xpage);
                }
                int start, end;
                start = (page - 1) * numberpage;
                end = Math.min(page * numberpage, size);
                List<Product> listpage = pdb.getListByPage(listall, start, end);
                request.setAttribute("listpage", listpage);
                request.setAttribute("page", page);
                request.setAttribute("num", num);
                request.setAttribute("size", size);
            } catch (NumberFormatException e) {
                System.out.println(e);
            }
            break;
            case "high":
                try {
                ca = (caId == null) ? 0 : Integer.parseInt(caId);

                List<Product> listall;

                listall = pdb.sort(ca, sortpro);

                int page, numberpage = 6;
                int size = listall.size();
                int num = (size % numberpage == 0 ? (size / numberpage) : ((size / numberpage) + 1));
                String xpage = request.getParameter("page");
                if (xpage == null) {
                    page = 1;
                } else {
                    page = Integer.parseInt(xpage);
                }
                int start, end;
                start = (page - 1) * numberpage;
                end = Math.min(page * numberpage, size);
                List<Product> listpage = pdb.getListByPage(listall, start, end);
                request.setAttribute("listpage", listpage);
                request.setAttribute("page", page);
                request.setAttribute("num", num);
                request.setAttribute("size", size);
            } catch (NumberFormatException e) {
                System.out.println(e);
            }
            break;
            default:
                break;
        }

        request.setAttribute("list", list);
        request.setAttribute("list1", list1);
        request.setAttribute("list_img", list_img);
        request.getRequestDispatcher("../view/Sort.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("../view/Sort.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
