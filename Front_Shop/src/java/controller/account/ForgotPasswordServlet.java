/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.account;

import dal.AccountDAO;
import helper.GetRandom;
import helper.SendMail;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import models.Account;
import utlis.GetParam;

/**
 *
 * @author dinhd513
 */
public class ForgotPasswordServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @return
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected boolean processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        String gmail = GetParam.getStringParam(request, "gmailToForgot", "Gmail",
                "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$", "This is not gmail", 5, 50, null);

        AccountDAO dao = new AccountDAO();
        Account acc = dao.searchAccByID(gmail);

        if (gmail == null || acc == null) {
            return false;

        }
        return true;

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setAttribute("mess", "Your gmail is not esist");
        request.getRequestDispatcher("../view/ForgotPassword.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (processRequest(request, response) == true) {

            String gmail = GetParam.getStringParam(request, "gmailToForgot", "Gmail",
                    "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$", "This is not gmail", 5, 50, null);

            AccountDAO dao = new AccountDAO();
            Account acc = dao.searchAccByID(gmail);

            String pass = GetRandom.generateRandomNumber(8);
            
            dao.setPasswordAccount(acc.getGmail(), pass);

            String subject = "New Password OF Front Website";
            String text = "Hello\n"
                    + "\n"
                    + "Your new password has been updated and here is your password: \n " + pass + "\n"
                    + "\n Please change this password after logging in"
                    + "\n "
                    + "Kind Regards, Front";
            SendMail send = new SendMail();
            send.SendGmail(acc.getGmail(), subject, text);
            
            request.setAttribute("mess", "Password has been sent to your gmail");
            request.getRequestDispatcher("../view/ForgotPassword.jsp").forward(request, response);
        }
        this.doGet(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
