/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import models.Product;
import models.ProductImage;

/**
 *
 * @author ACER
 */
public class ProductDAO {
    Connection conn = null;        // keets noois sql
    PreparedStatement ps = null;  // nems cau lenh query
    ResultSet rs = null;    // keet qua tra ve

    public List<Product> getAll() {
        List<Product> list = new ArrayList<>();
        String sql = "select * from Product";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Product p = new Product();
                p.setProID(rs.getInt("proID"));
                p.setProName(rs.getString("proName"));
                p.setProDetail(rs.getString("proDetail"));
                p.setProPrice(rs.getFloat("proPrice"));
                p.setCaID(rs.getInt("caID"));
                p.setProStatus(rs.getBoolean("proStatus"));
                list.add(p);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Product> getProductByCategory(int id) {
        List<Product> list = new ArrayList<>();
        String sql = "select * from Product where caID=?";
        try {
            conn = new DBContext().getConnection();
            PreparedStatement st = conn.prepareStatement(sql);
            st.setInt(1, id);
            rs = st.executeQuery();
            while (rs.next()) {
                Product p = new Product();
                p.setProID(rs.getInt("proID"));
                p.setProName(rs.getString("proName"));
                p.setProDetail(rs.getString("proDetail"));
                p.setProPrice(rs.getFloat("proPrice"));
                p.setCaID(rs.getInt("caID"));
                p.setProStatus(rs.getBoolean("proStatus"));
                list.add(p);
                list.add(p);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Product> getListByPage(List<Product> list, int start, int end) {
        ArrayList<Product> arr = new ArrayList<>();
        for (int i = start; i < end; i++) {
            arr.add(list.get(i));
        }
        return arr;
    }

    public List<Product> sort(int id, String sort) {
        List<Product> list = new ArrayList<>();
        if (sort.equals("high")) {
            if (id != 0) {
                String sql = "select * from Product where caID=?"
                        + " order by proPrice ASC";
                try {
                    conn = new DBContext().getConnection();
                    PreparedStatement st = conn.prepareStatement(sql);
                    st.setInt(1, id);
                    rs = st.executeQuery();
                    while (rs.next()) {
                        Product p = new Product();
                        p.setProID(rs.getInt("proID"));
                        p.setProName(rs.getString("proName"));
                        p.setProDetail(rs.getString("proDetail"));
                        p.setProPrice(rs.getFloat("proPrice"));
                        p.setCaID(rs.getInt("caID"));
                        p.setProStatus(rs.getBoolean("proStatus"));
                        list.add(p);
                        list.add(p);
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            } else {
                String sql = "select * from Product "
                        + " order by proPrice ASC";
                try {
                    conn = new DBContext().getConnection();
                    PreparedStatement st = conn.prepareStatement(sql);

                    rs = st.executeQuery();
                    while (rs.next()) {
                        Product p = new Product();
                        p.setProID(rs.getInt("proID"));
                        p.setProName(rs.getString("proName"));
                        p.setProDetail(rs.getString("proDetail"));
                        p.setProPrice(rs.getFloat("proPrice"));
                        p.setCaID(rs.getInt("caID"));
                        p.setProStatus(rs.getBoolean("proStatus"));
                        list.add(p);
                        list.add(p);
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        } else if(sort.equals("low")){
            if (id != 0) {
                String sql = "select * from Product where caID=?"
                        + " order by proPrice DESC";
                try {
                    conn = new DBContext().getConnection();
                    PreparedStatement st = conn.prepareStatement(sql);
                    st.setInt(1, id);
                    rs = st.executeQuery();
                    while (rs.next()) {
                        Product p = new Product();
                        p.setProID(rs.getInt("proID"));
                        p.setProName(rs.getString("proName"));
                        p.setProDetail(rs.getString("proDetail"));
                        p.setProPrice(rs.getFloat("proPrice"));
                        p.setCaID(rs.getInt("caID"));
                        p.setProStatus(rs.getBoolean("proStatus"));
                        list.add(p);
                        list.add(p);
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            } else {
                String sql = "select * from Product "
                        + " order by proPrice DESC";
                try {
                    conn = new DBContext().getConnection();
                    PreparedStatement st = conn.prepareStatement(sql);

                    rs = st.executeQuery();
                    while (rs.next()) {
                        Product p = new Product();
                        p.setProID(rs.getInt("proID"));
                        p.setProName(rs.getString("proName"));
                        p.setProDetail(rs.getString("proDetail"));
                        p.setProPrice(rs.getFloat("proPrice"));
                        p.setCaID(rs.getInt("caID"));
                        p.setProStatus(rs.getBoolean("proStatus"));
                        list.add(p);
                        list.add(p);
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        } else{
            if (id != 0) {
                String sql = "select * from Product where caID=?"
                        + " order by proPrice DESC";
                try {
                    conn = new DBContext().getConnection();
                    PreparedStatement st = conn.prepareStatement(sql);
                    st.setInt(1, id);
                    rs = st.executeQuery();
                    while (rs.next()) {
                        Product p = new Product();
                        p.setProID(rs.getInt("proID"));
                        p.setProName(rs.getString("proName"));
                        p.setProDetail(rs.getString("proDetail"));
                        p.setProPrice(rs.getFloat("proPrice"));
                        p.setCaID(rs.getInt("caID"));
                        p.setProStatus(rs.getBoolean("proStatus"));
                        list.add(p);
                        list.add(p);
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            } else {
                String sql = "select * from Product "
                        + " order by proPrice DESC";
                try {
                    conn = new DBContext().getConnection();
                    PreparedStatement st = conn.prepareStatement(sql);

                    rs = st.executeQuery();
                    while (rs.next()) {
                        Product p = new Product();
                        p.setProID(rs.getInt("proID"));
                        p.setProName(rs.getString("proName"));
                        p.setProDetail(rs.getString("proDetail"));
                        p.setProPrice(rs.getFloat("proPrice"));
                        p.setCaID(rs.getInt("caID"));
                        p.setProStatus(rs.getBoolean("proStatus"));
                        list.add(p);
                        list.add(p);
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return list;
    }

    public static void main(String[] args) {
        List<Product> list = new ArrayList<>();
        ProductDAO pidb = new ProductDAO();
        list = pidb.sort(0,"high");

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i).getProID());
        }
    }
}
