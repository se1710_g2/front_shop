/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import models.Account;
import models.Product;
import models.ProductImage;

/**
 *
 * @author ACER
 */
public class ProductImageDAO {
    
    Connection conn = null;        // keets noois sql
    PreparedStatement ps = null;  // nems cau lenh query
    ResultSet rs = null;    // keet qua tra ve
    
    public ProductImage getImageById(int id) {
        
        String sql = "select top 1 * from [Product Image] where proId=?";
        ProductImage p = new ProductImage();
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
             rs = ps.executeQuery();
            while (rs.next()) {     
                p.setProID(rs.getInt("proID"));
                p.setProImg(rs.getString("proImg"));     
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return p;
    }
    
    public List<ProductImage> getImagetop1() {
        List<ProductImage> list = new ArrayList<>();
        String sql = "select top 1 * from [Product Image]";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add( new ProductImage(rs.getInt(1),
                        rs.getString(2))
                        
                );
            }
        } catch (Exception e) {

        }
        return list;

    }
    
}
