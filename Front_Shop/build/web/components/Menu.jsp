<%-- 
    Document   : Menu.jsp
    Created on : May 20, 2023, 3:29:59 PM
    Author     : dinhd513
--%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="wel">
    <h6 href=""><strong>Welcome</strong> to our shop</h6>

</div>
<!-- Top navigation -->
<div class="topnav">

    <!-- Centered link -->
    <div class="topnav-centered">
        <a href="home.html" class="active"
           style="color: black; font-size: 40px; font-family: 'MuseoModerno', cursive;"><strong>Front:</strong></a>
    </div>

    <!-- Left-aligned links (default) -->
    <a href="shopAll.html">Shop All</a>
    <a href="#contact">Blog</a>
    <a href="#about">Combo</a>
    <!-- Right-aligned links -->

    <div class="topnav-right">
        <a style="font-size: 20px;margin-left: 5px;" href="cart.html">
            <i class="fa fa-shopping-cart" style="font-size:25px; height: fit-content;"></i>10</a>
        
            <c:if test="${sessionScope.account != null}">
            <a href="">Profile</a>
            <a href="../view/logout">Log out</a>   
            </c:if>
         
            <c:if test="${sessionScope.account == null}">
         
                <a href="../view/Login.jsp">Log in</a>   
            </c:if>
            
           
        
    </div>

</div>
