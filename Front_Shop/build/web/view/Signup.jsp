<%-- 
    Document   : signUp
    Created on : May 19, 2023, 2:09:07 PM
    Author     : dinhd513
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sign up Page</title>

        <link rel="stylesheet" href="../stylesheet/login.css">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet">
    </head>
    <body>

        <h2 style="font-size: 55px; font-family: 'Lobster', cursive; margin-top: 100px">Front</h2>
        <div class="container" style=" margin: auto">
            <form action="../view/signup" method="post" style="width: 100%">
                <div class="row">
                    <h2 style="text-align:center">Sign Up</h2>
                    <div class="col">

                        <p>${mess}</p>

                        <jsp:include page="../components/inputField.jsp">
                            <jsp:param name="type" value="text"/>
                            <jsp:param name="placeholder" value="Full Name"/>
                            <jsp:param name="field" value="name"/>
                        </jsp:include>
                        <div style="display: flex; justify-content: center; width: 20%">
                        <input type="radio" id="nam" name="gender" value="1"  required>
                        <label for="nam">Male</label> 
                        <input type="radio" id="nu" name="gender" value="0" required>
                        <label for="nu">Female</label>
                        </div>
                        
                        <jsp:include page="../components/inputField.jsp">
                            <jsp:param name="type" value="text"/>
                            <jsp:param name="placeholder" value="Gmail"/>
                            <jsp:param name="field" value="gmail"/>
                        </jsp:include>

                        <jsp:include page="../components/inputField.jsp">
                            <jsp:param name="type" value="password"/>
                            <jsp:param name="placeholder" value="Password"/>
                            <jsp:param name="field" value="password"/>
                        </jsp:include>

                        <input type="submit" value="Sign Up">
                    </div>  

                </div>
            </form>
        </div>

        <div class="bottom-container">
            <div class="row">
                <div class="col">
                    <a href="../view/Login.jsp" style="color:rgb(0, 0, 0)" class="btn">Go back</a>
                </div>
                
            </div>
        </div>



    </body>
</html>
