<%-- 
    Document   : FotgotPassword
    Created on : May 20, 2023, 10:56:31 AM
    Author     : dinhd513
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        
        <link rel="stylesheet" href="../stylesheet/login.css">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet">
    </head>
    <body>
         <h2 style="font-size: 55px; font-family: 'Lobster', cursive;">Front</h2>
        <div class="container">
            <form action="../view/forgotPassword" method="post">
                <div class="row">
                    <h2 style="text-align:center">Enter your Gmail</h2>
                    <div class="col">
                        
                        <p>${mess}</p>
                        <jsp:include page="../components/inputField.jsp">
                            <jsp:param name="type" value="text"/>
                            <jsp:param name="placeholder" value="Gmail"/>
                            <jsp:param name="field" value="gmailToForgot"/>
                        </jsp:include>
                    <input type="submit" value="Get Password">
                    </div>  

                </div>
            </form>
        </div>

        <div class="bottom-container">
            <div class="row">
                <div class="col">
                    <a href="../view/Signup.jsp" style="color:rgb(0, 0, 0)" class="btn">Go back</a>
                </div>
            </div>
        </div>
    </body>
</html>
